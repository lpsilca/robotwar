function iaGenerator(mapSize) {
    return {
        friendWin: {
            value: false,
            position: null
        },
        hasTeleportToWin: false,
        getName: function() {
            return `felix`;
        },

        onFriendWins: function(exit) {
            //exit est la position de la sortie { x: .., y: .. }
            this.friendWin.value = true;
            this.friendWin.position = exit;
        },

        onResponseX: function(hPosition) {
            //1 je suis trop à gauche
            //-1 je suis trop à droite
            //0 je suis en face de la sortie
            // console.log(hPosition);
        },

        onResponseY: function(vPosition) {
            //1 je suis trop bas
            //-1 je suis trop haut
            //0 je suis en face de la sortie
            // console.log(vPosition);
        },
        moveNextToExit: function() {
            this.hasTeleportToWin = true;
            var moveX;
            switch (this.friendWin.position.x) {
                case mapSize - 1:
                    moveX = this.friendWin.position.x - 1;
                    break;
                default:
                    moveX = this.friendWin.position.x + 1;
                    break;
            }
            return {
                action: "teleport",
                params: {
                    x: moveX,
                    y: this.friendWin.position.y
                }
            };
        },
        teleportToMiddle: {
            action: "teleport",
            params: {
                x: Math.round(mapSize / 2),
                y: Math.round(mapSize / 2)
            }
        },
        action: function(position, round) {
            if (round === 0)
                return this.teleportToMiddle;

            if (this.hasTeleportToWin) {
                var moveX;
                switch (this.friendWin.position.x) {
                    case mapSize - 1:
                        moveX = 1;
                        break;
                    default:
                        moveX = -1;
                        break;
                }
                return {
                    action: "move",
                    params: {
                        dx: parseInt(moveX),
                        dy: 0
                    }
                };
            }

            if (this.friendWin.value)
                return this.moveNextToExit();


            var dx = Math.random() - 0.5;
            var dy = Math.random() - 0.5;

            var move = {
                action: "move",
                params: {
                    dx: dx,
                    dy: dy
                }
            };
            
            return move;
        }
    };
}

module.exports = iaGenerator;