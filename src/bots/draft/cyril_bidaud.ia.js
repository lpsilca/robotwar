function iaGenerator(mapSize) {

    var friendWin = {
        value: false,
        position: null
    };
    var hasTeleportToWin = false;

    var moveNextToExit = function() {
        hasTeleportToWin = true;
        var moveX;
        switch (friendWin.position.x) {
            case mapSize - 1:
                moveX = friendWin.position.x - 1;
                break;
            default:
                moveX = friendWin.position.x + 1;
                break;
        }
        return {
            action: "teleport",
            params: {
                x: moveX,
                y: friendWin.position.y
            }
        };
    };
    var teleportToMiddle = {
        action: "teleport",
        params: {
            x: Math.round(mapSize / 2),
            y: Math.round(mapSize / 2)
        }
    };

    return {
        getName: function() {
            return `YEPZY`;
        },

        onFriendWins: function(exit) {
            //exit est la position de la sortie { x: .., y: .. }
            friendWin.value = true;
            friendWin.position = exit;
        },

        onResponseX: function(hPosition) {
            //1 je suis trop à gauche
            //-1 je suis trop à droite
            //0 je suis en face de la sortie
            // console.log(hPosition);
        },

        onResponseY: function(vPosition) {
            //1 je suis trop bas
            //-1 je suis trop haut
            //0 je suis en face de la sortie
            // console.log(vPosition);
        },
        action: function(position, round) {
            if (round === 0)
                return teleportToMiddle;

            if (hasTeleportToWin) {
                var moveX;
                switch (friendWin.position.x) {
                    case mapSize - 1:
                        moveX = 1;
                        break;
                    default:
                        moveX = -1;
                        break;
                }
                return {
                    action: "move",
                    params: {
                        dx: parseInt(moveX),
                        dy: 0
                    }
                };
            }

            if (friendWin.value)
                return moveNextToExit();


            var dx = Math.random() - 0.5;
            var dy = Math.random() - 0.5;

            var move = {
                action: "move",
                params: {
                    dx: dx,
                    dy: dy
                }
            };

            return move;
        }
    };
}

module.exports = iaGenerator;