function iaGenerator(mapSize) {


    var response = "";
    var nbTime = 0;

    var stepX = mapSize * 0.2;
    var stepY = mapSize * 0.2;

    var order = "";

    var friendWin = {
        value: false,
        position: null
    };
    var hasTeleportToWin = false;

    var moveNextToExit = function() {
        hasTeleportToWin = true;
        var moveX;
        switch (friendWin.position.x) {
            case mapSize - 1:
                moveX = friendWin.position.x - 1;
                break;
            default:
                moveX = friendWin.position.x + 1;
                break;
        }
        return {
            action: "teleport",
            params: {
                x: moveX,
                y: friendWin.position.y
            }
        };
    };
    var teleportToMiddle = {
        action: "teleport",
        params: {
            x: Math.round(mapSize / 2),
            y: Math.round(mapSize / 2)
        }
    };

    var addOrder = function (newOrder){
      order.length == 0 ? order = newOrder : order += "-"+newOrder;
    };


    return {
        getName: function () {
            return "Ganach";
        },

        onFriendWins: function (exit) {
            //exit est la position de la sortie { x: .., y: .. }
            friendWin.value = true;
            friendWin.position = exit;
        },

        onResponseX: function (hPosition) {
            //1 je suis trop à gauche
            //-1 je suis trop à droite
            //0 je suis en face de la sortie
            switch (hPosition){
              case 1:
                addOrder("R");
                break;
              case -1:
                addOrder("L");
                break;
              default:
                addOrder("N");
                break;
            }
        },

        onResponseY: function (vPosition) {
            //1 je suis trop bas
            //-1 je suis trop haut
            //0 je suis en face de la sortie
            switch (vPosition){
              case 1:
                addOrder("D");
                break;
              case -1:
                addOrder("U");
                break;
              default:
                addOrder("N");
                break;
            }
        },

        action: function (position, round) {

          if (round === 0)
              return teleportToMiddle;

          if (hasTeleportToWin) {
              var moveX;
              switch (friendWin.position.x) {
                  case mapSize - 1:
                      moveX = 1;
                      break;
                  default:
                      moveX = -1;
                      break;
              }
              return {
                  action: "move",
                  params: {
                      dx: parseInt(moveX),
                      dy: 0
                  }
              };
          }

          if (friendWin.value)
              return moveNextToExit();


            var dx = "";
            var dy = "";



            var ask = {
              action: "ask",
              params: "x"
            };
            return ask;
            var ask = {
              action: "ask",
              params: "y"
            };

              let [orderX,orderY] = order.split("-");
              if (nbTime <= stepX){
                switch (orderX){
                  case "R":
                    dx = 1;
                    break;
                  case "L":
                    dx = -1;
                    break;
                  default:
                    dx = 0;
                }
              }
              if (nbTime <= stepY){
                switch (orderY){
                  case "U":
                    dy = -1;
                    break;
                  case "D":
                    dy = 1;
                    break;
                  default:
                    dy = 0;
                }
              }
              nbTime ++;

                var move = {
                    action: "move",
                    params: {
                        dx: dx,
                        dy: dy
                    }
                };

                return move;
              }
              else {
                nbTime = 0;
                return ask;
              }









            /** OU
            var ask = {
                action: "ask",
                params: "x"
            };

            var teleport = {
                action: "teleport",
                params: {
                    x: Math.round(Math.random() * mapSize),
                    y: Math.round(Math.random() * mapSize)
                }
            };
            */


        }
    };
}

module.exports = iaGenerator;
