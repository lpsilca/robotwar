function iaGenerator(mapSize) {
    var countMove = 0; //number of move

    var stepX = mapSize * 0.2 - 1; //move 20% of the map
    var stepY = mapSize * 0.2 - 1; //move 20% of the map

    //Store the orders given
    var order = {
        dx: null,
        dy: null
    };

    //Store the last orders to check if the target is exceed
    var lastOrder = {
        dx: null,
        dy: null
    };

    //Store if a teammate has found the target and where the target is
    var teammateWin = {
        value: false,
        position: null
    };

    //Check if have been teleport next to the target
    var hasTeleportToWin = false;

    //Teleport our player next to the target and not out of bound
    var moveNextToExit = () => {
        hasTeleportToWin = true;
        let moveX;
        switch (teammateWin.position.x) {
            case mapSize - 1:
                moveX = teammateWin.position.x - 1;
                break;
            default:
                moveX = teammateWin.position.x + 1;
                break;
        }
        return {
            action: "teleport",
            params: {
                x: moveX,
                y: teammateWin.position.y
            }
        };
    };

    //Teleport the player in the middle of the map
    var teleportInMiddle = {
        action: "teleport",
        params: {
            x: Math.round(mapSize / 2),
            y: Math.round(mapSize / 2)
        }
    };

    //Modify the step value if we exceed the target set as 1
    var modifStep = () => {
        if (lastOrder.dx != null && lastOrder.dy != null) {
            if (order.dx != 0 && order.dy != 0) {
                if (lastOrder.dx != order.dx)
                    stepX = 1;
                if (lastOrder.dy != order.dy)
                    stepY = 1;
            } else {
                stepX = 1000;
                stepY = 1000;
            }
        }
    };

    //Prevent if the next move will go out of bound and change the step
    var noGoOut = (position) => {
        console.log("nextX " + (position.x + (order.dx * 2)));
        if ((position.x + (order.dx * 2) >= mapSize - 1 || position.x + (order.dx * 2) < 0))
            stepX = 1;
        console.log("nextY " + (position.y + (order.dy * 2)));
        if ((position.y + (order.dy * 2) >= mapSize - 1 || position.y + (order.dy * 2) <= 0))
            stepY = 1;
    }

    //Change the order by axis
    var setOrder = (newOrder, type) => {
        type == "x" ? order.dx = parseInt(newOrder) : order.dy = parseInt(newOrder);
    };

    //Ask to get the orders
    var fillOrder = () => {
        if (order.dx === null)
            return {
                action: "ask",
                params: "x"
            };
        else
            return {
                action: "ask",
                params: "y"
            };
    };

    //Copy the current order by axis to check if it will change
    var copyOrder = (type) => {
        type == "x" ? lastOrder.dx = order.dx : lastOrder.dy = order.dy;
    };

    return {
        getName: function() {
            return "BID-GAN";
        },

        onFriendWins: function(exit) {
            //exit est la position de la sortie { x: .., y: .. }
            teammateWin.value = true;
            teammateWin.position = exit;
        },

        onResponseX: function(hPosition) {
            //1 je suis trop à gauche
            //-1 je suis trop à droite
            //0 je suis en face de la sortie
            setOrder(hPosition, "x");
        },

        onResponseY: function(vPosition) {
            //1 je suis trop bas
            //-1 je suis trop haut
            //0 je suis en face de la sortie
            setOrder(vPosition, "y");
        },

        action: function(position, round) {
            //First round go in the middle
            if (round === 0)
                return teleportInMiddle;

            //If we have been teleport next to the target
            if (hasTeleportToWin) {
                let moveX;
                switch (teammateWin.position.x) {
                    case mapSize - 1:
                        moveX = 1;
                        break;
                    default:
                        moveX = -1;
                        break;
                }
                return {
                    action: "move",
                    params: {
                        dx: parseInt(moveX),
                        dy: 0
                    }
                };
            }

            //If a teammate has found the target
            if (teammateWin.value)
                return moveNextToExit();

            //Fill the order, if one of them is empty
            if (order.dx === null || order.dy === null) {
                return fillOrder();
            }

            //If we exceed, set step as 1 or if we are align, step unlimited
            modifStep();

            //Avoid move out of bound
            noGoOut(position);


            //Set move order
            let dx = order.dx;
            let dy = order.dy;

            // DEBUG
            // console.log("countMove " + countMove + "| stepX " + stepX + "| stepY " + stepY);
            // console.log("order x " + order.dx + "| order y " + order.dy + "| lastOrder x " + lastOrder.dx + "| lastOrder y " + lastOrder.dy);
            // console.log('dx ' + dx + ' dy ' + dy)

            //Reinitiate order if the step is exceed
            if (countMove >= stepX) {
                copyOrder("x");
                order.dx = order.dx != 0 ? null : 0;
            }
            if (countMove >= stepY) {
                copyOrder("y");
                order.dy = order.dy != 0 ? null : 0;
            }
            //If the two steap are exceed reinitiate counter
            if (countMove >= Math.max(stepX, stepY)) {
                countMove = 0;
            } else
                countMove++;

            return {
                action: "move",
                params: {
                    dx: dx,
                    dy: dy
                }
            };
        }
    };
}

module.exports = iaGenerator;